package dunstctl

import (
	"github.com/spf13/cobra"
	print "gitlab.com/fizzycfg/chips/airfrier/pkg/print"
	"gitlab.com/fizzycfg/chips/dunstctl/internal"
)

var (
	resumeCmd = &cobra.Command{
		Use:   "resume",
		Short: "Resume dunst notifications",
		RunE:  resume,
	}
)

func init() {
	// Add subcommand
	MainCmd.AddCommand(resumeCmd)
}

func resume(command *cobra.Command, args []string) error {
	err := internal.NotifySend("DUNST_COMMAND_RESUME")
	if err != nil {
		print.Fatal("failed to resume notifications")
	}
	print.Success("notifications resumed")
	return nil
}
