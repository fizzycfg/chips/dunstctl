package dunstctl

import (
	"github.com/spf13/cobra"
	config "gitlab.com/fizzycfg/chips/airfrier/pkg/config"
)

var verbose bool

// MainCmd represents the base command when called without any subcommands
var MainCmd = &cobra.Command{
	Use:   "dunstctl",
	Short: "Dunst controller",
	Long:  "dunstctl allows to control dunst notification application at runtime",
}

func init() {
	cobra.OnInitialize(func() { config.Init(MainCmd.Name()) })
	// Add generic flags
	MainCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "verbose output")
	// Bind configuration with flags
	config.AddFlags(MainCmd)
}
