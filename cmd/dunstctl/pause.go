package dunstctl

import (
	"github.com/spf13/cobra"
	print "gitlab.com/fizzycfg/chips/airfrier/pkg/print"
	"gitlab.com/fizzycfg/chips/dunstctl/internal"
)

var (
	pauseCmd = &cobra.Command{
		Use:   "pause",
		Short: "Pause dunst notifications",
		RunE:  pause,
	}
)

func init() {
	// Add subcommand
	MainCmd.AddCommand(pauseCmd)
}

func pause(command *cobra.Command, args []string) error {
	err := internal.NotifySend("DUNST_COMMAND_PAUSE")
	if err != nil {
		print.Fatal("failed to pause notifications")
	}
	print.Success("notifications paused")
	return nil
}
