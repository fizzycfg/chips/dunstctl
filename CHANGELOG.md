# ChangeLog

## 0.1.4

## Development changes

- Use `exec.CheckProgram` from Airfrier library

## 0.1.3

### Development changes

- Update dependencies

## 0.1.2

### Development changes

- Use `airfrier` library

## 0.1.1

### User changes

- Add support for `verbose` flag

### Development changes

- Move `main.go` in root directory
- Improve `CHANGELOG.md` structure

## 0.1.0

### User changes

First working version.
Sub-commands available:

- `pause`
- `resume`
