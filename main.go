package main

import (
	"gitlab.com/fizzycfg/chips/airfrier/pkg/print"
	"gitlab.com/fizzycfg/chips/dunstctl/cmd/dunstctl"
)

func main() {
	if err := dunstctl.MainCmd.Execute(); err != nil {
		print.Fatal(err)
	}
}
