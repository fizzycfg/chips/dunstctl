BUILDDIR := $(CURDIR)/build
BINNAME  ?= dunstctl
SRC      := $(shell find . -type f -name '*.go' -print)

.PHONY: all
all: build

.PHONY: build
build: $(BUILDDIR)/$(BINNAME)

$(BUILDDIR)/$(BINNAME): $(SRC)
	go build -o $(BUILDDIR)/$(BINNAME)
	chmod +x $(BUILDDIR)/$(BINNAME)
