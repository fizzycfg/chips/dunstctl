module gitlab.com/fizzycfg/chips/dunstctl

go 1.14

require (
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/spf13/cobra v1.0.0
	gitlab.com/fizzycfg/chips/airfrier v0.1.2
)
