package internal

import (
	"os/exec"

	airfrierExec "gitlab.com/fizzycfg/chips/airfrier/pkg/exec"
)

// NotifySend executes "notify-send" with provided arguments
func NotifySend(arg ...string) error {
	if err := airfrierExec.CheckProgram("notify-send"); err != nil {
		return err
	}

	_, err := exec.Command("notify-send", arg...).CombinedOutput()

	return err
}
